data = read.csv("abalone.data", header = F)
colnames(data) <- c("Sex", "Length", "Diameter", "Height", "Whole weight", "Shucked weight", "Viscera weight", "Shell weight", "Class_Rings")
attach(data)

#Question 1
linear_model_q1 <- lm(Diameter~Length)
plot(Diameter~Length)
abline(linear_model_q1)
linear_model_q1
summary(linear_model_q1)
cor(Length, Diameter)

#Question 2
multilinear_model_q2 <- lm(`Whole weight`~`Shucked weight`+`Viscera weight`+`Shell weight`)
multilinear_model_q2
summary(multilinear_model_q2)
pairs(`Whole weight`~`Shucked weight`+`Viscera weight`+`Shell weight`)
sum_weight = `Shucked weight`+`Viscera weight`+`Shell weight`
temp_lm <- lm(`Whole weight`~sum_weight)
temp_lm
plot(`Whole weight`~sum_weight)
abline(temp_lm)

#Question 3
#a
linear_model_q3a <- lm(`Whole weight`~Diameter)
plot(`Whole weight`~Diameter)
abline(linear_model_q3a, col = 'purple')
linear_model_q3a
summary(linear_model_q3a)
cor(`Whole weight`, Diameter)

#b
linear_model_q3b <- lm(`Whole weight`~Diameter+I(Diameter^2))
linear_model_q3b
plot(`Whole weight`~Diameter)
eq_b = function(x){linear_model_q3b[["coefficients"]][["(Intercept)"]]+linear_model_q3b[["coefficients"]][["Diameter"]]*x+linear_model_q3b[["coefficients"]][["I(Diameter^2)"]]*(x^2)}
curve(eq_b, col='blue', add = TRUE)
summary(linear_model_q3b)
cor(`Whole weight`, Diameter+I(Diameter^2))

#c
linear_model_q3c <- lm(`Whole weight`~I(Diameter^3))
linear_model_q3c
plot(`Whole weight`~Diameter)
eq_c = function(x){linear_model_q3c[["coefficients"]][["(Intercept)"]]+linear_model_q3c[["coefficients"]][["I(Diameter^3)"]]*(x^3)}
curve(eq_c, col='red', add = TRUE)
summary(linear_model_q3c)
cor(`Whole weight`, I(Diameter^3))

#d
linear_model_q3d <- lm(log(`Whole weight`)~Diameter)
linear_model_q3d
plot(`Whole weight`~Diameter)
eq_d = function(x){exp(linear_model_q3d[["coefficients"]][["(Intercept)"]]+linear_model_q3d[["coefficients"]][["Diameter"]]*x)}
curve(eq_d, col='brown', add = TRUE)
summary(linear_model_q3d)
cor(log(`Whole weight`), Diameter )


#Question 4
is_inf <- as.factor(ifelse(Sex=='I','1','0'))
#a
plot(is_inf~Length)
logi_model_q4a <- glm(is_inf~Length, family = "binomial")
logi_model_q4a
summary(logi_model_q4a)
pairs_q4a <-(paste(round(predict(logi_model_q4a, type="response")), is_inf))
table_q4a <- table(pairs_q4a)
table_q4a
correct_q4a <- table_q4a[1]+table_q4a[4]
acc_q4a <- correct_q4a/length(Sex)

#b
plot(is_inf~`Whole weight`)
logi_model_q4b <- glm(is_inf~`Whole weight`, family = "binomial")
logi_model_q4b
summary(logi_model_q4b)
pairs_q4b <-(paste(round(predict(logi_model_q4b, type="response")), is_inf))
table_q4b <- table(pairs_q4b)
table_q4b
correct_q4b <- table_q4b[1]+table_q4b[4]
acc_q4b <- correct_q4b/length(Sex)

#c
plot(is_inf~data$Class_Rings)
logi_model_q4c <- glm(is_inf~data$Class_Rings, family = "binomial")
logi_model_q4c
summary(logi_model_q4c)
pairs_q4c <-(paste(round(predict(logi_model_q4c, type="response")), is_inf))
table_q4c <- table(pairs_q4c)
table_q4c
correct_q4c <- table_q4c[1]+table_q4c[4]
acc_q4c <- correct_q4c/length(Sex)

#d
logi_model_q4d <- glm(is_inf~Length+`Whole weight`+data$Class_Rings, family = "binomial")
logi_model_q4d
plot(is_inf~Length+`Whole weight`+data$Class_Rings)
summary(logi_model_q4d)
pairs_q4d <-(paste(round(predict(logi_model_q4d, type="response")), is_inf))
table_q4d <- table(pairs_q4d)
table_q4d
correct_q4d <- table_q4d[1]+table_q4d[4]
acc_q4d <- correct_q4d/length(Sex)

#Question 5
data_q5 = read.csv("adult.data", header = F)
colnames(data_q5) <- c("Age", "Work Class", "fnlwgt", "Education", "Education num", "Martial status", "Occupation", "Relationship", "Race", "Sex", "Capital gain", "Capital loss", "Hour per week", "Native country", "Salary")
logi_model_q5 <- glm(data_q5$Sex~data_q5$`Capital gain`+data_q5$`Capital loss`+data_q5$`Age`+data_q5$`Work Class`+data_q5$`fnlwgt`+data_q5$`Education`+data_q5$`Education num`+data_q5$`Martial status`+data_q5$`Occupation`+data_q5$`Relationship`+data_q5$`Race`+data_q5$`Hour per week`+data_q5$`Native country`+data_q5$`Salary`, family = "binomial")
logi_model_q5
pairs_q5 <-(paste(round(predict(logi_model_q5, type="response")), data_q5$Sex))
table_q5 <- table(pairs_q5)
table_q5

pairs(data_q5$Sex~data_q5$`Capital gain`+data_q5$`Capital loss`+data_q5$`Age`+data_q5$`Work Class`+data_q5$`fnlwgt`+data_q5$`Education`+data_q5$`Education num`+data_q5$`Martial status`+data_q5$`Occupation`+data_q5$`Relationship`+data_q5$`Race`+data_q5$`Hour per week`+data_q5$`Native country`+data_q5$`Salary`, horInd = 1)

